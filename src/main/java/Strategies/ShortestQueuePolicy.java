package Strategies;

import Mechanics.Server;
import Mechanics.Task;
import Strategies.SelectionPolicy;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ShortestQueuePolicy implements SelectionPolicy
{
    @Override
    public void addTask(List<Server> servers, Task t)
    {
        //get all the queues' size and add to the shortest one
        List<Server> aux = new LinkedList<>(servers);

        Collections.sort(aux, Comparator.comparingInt(Server::getSize));

        ListIterator<Server> it = aux.listIterator();

        Server toAdd = null;
        int index = -1;

        boolean flag = true;

        while(it.hasNext())
        {
            Server current = it.next();

            if(current.addTask(t) != -1)
            {
                toAdd = current;
                flag = false;
                break;
            }
        }

        if(toAdd != null)
        {
            for (int i = 0; i < servers.size(); i++)
            {
                if (servers.get(i).equals(toAdd))
                {
                    index = i;
                    break;
                }
            }
        }

        t.setServerIndex(index);

        if(flag || index == -1)
        {
            Logger.getGlobal().log(Level.INFO, "Could not add task " + t.toString() + "to any of the servers");
        }

        else {
            Logger.getGlobal().log(Level.INFO, "Client " + t.toString() + " Entered to Server " + t.getServerIndex());
        }
    }
}
