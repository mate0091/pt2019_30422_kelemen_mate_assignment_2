package Strategies;

import Mechanics.Server;
import Mechanics.Task;

import java.util.List;

public interface SelectionPolicy
{
    public void addTask(List<Server> servers, Task t);
}
