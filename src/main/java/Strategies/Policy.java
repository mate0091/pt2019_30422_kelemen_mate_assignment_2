package Strategies;

public enum Policy
{
    SHORTEST_QUEUE(),
    SHORTEST_SERVICE_TIME()
}
