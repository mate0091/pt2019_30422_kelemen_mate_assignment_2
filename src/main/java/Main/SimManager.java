package Main;

import Main.GUI;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.*;
import java.util.logging.Formatter;

import Mechanics.*;
import Strategies.*;

public class SimManager implements Runnable
{
    private int timeLimit = 100;
    private int maxProcessingTime = 6;
    private int minProcessingTime = 2;
    private int nrServers = 1;
    private int minArrivalTimeDelta = 0;
    private int maxArrivalTimeDelta = 0;

    private AtomicInteger peakTime;
    private AtomicInteger peakClients;

    private AtomicBoolean isDone;

    private Scheduler scheduler;
    private GUI gui;

    private AtomicInteger currentTime;
    private Random r;
    private AtomicInteger clientIndex;
    private AtomicInteger coolDown;

    public SimManager(GUI gui, int timeLimit, int minProcessingTime, int maxProcessingTime, int nrServers, int maxClientPerServer, int minArrivalDelta, int maxArrivalDelta, Policy p)
    {
        this.timeLimit = timeLimit;
        this.minProcessingTime = minProcessingTime;
        this.maxProcessingTime = maxProcessingTime;
        this.nrServers = nrServers;
        this.peakTime = new AtomicInteger(0);
        this.peakClients = new AtomicInteger(Integer.MIN_VALUE);
        this.currentTime = new AtomicInteger(0);
        this.isDone = new AtomicBoolean(false);
        this.clientIndex = new AtomicInteger(1);
        this.minArrivalTimeDelta = minArrivalDelta;
        this.maxArrivalTimeDelta = maxArrivalDelta;

        this.coolDown = new AtomicInteger(0);
        this.r = new Random();

        try
        {
            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd_HH_mm_ss");
            FileHandler f = new FileHandler("./" + sdf.format(Calendar.getInstance().getTime()) + ".log");
            f.setFormatter(new Formatter() {
                @Override
                public String format(LogRecord record) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(record.getMessage());
                    sb.append("\n");

                    return sb.toString();
                }
            });
            Logger.getGlobal().addHandler(f);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        Logger.getGlobal().setLevel(Level.INFO);
        Logger.getGlobal().log(Level.INFO, "Simulation initialized".toUpperCase());

        if(scheduler != null) scheduler.killThreads();

        scheduler = new Scheduler(nrServers, maxClientPerServer, p);

        this.gui = gui;
    }

    @Override
    public void run()
    {
        currentTime.set(0);
        isDone.set(false);

        coolDown.set(r.nextInt(maxArrivalTimeDelta - minArrivalTimeDelta) + minArrivalTimeDelta);

        while(currentTime.get() < timeLimit && !isDone.get())
        {
            this.updatePeakTime();
            gui.displayData(serversToString(), currentTime.get(), isDone.get());

            scheduler.getServers().forEach(e ->
            {
                e.incEmptyQTime();
            });

            currentTime.incrementAndGet();
            coolDown.decrementAndGet();

            if(coolDown.get() == 0)
            {
                //generate task, reset cooldown
                Task t = Task.random(minProcessingTime, maxProcessingTime, currentTime.get(), clientIndex.get());

                Logger.getGlobal().info("Client " + t.toString() + " was generated");

                scheduler.dispatchTask(t);
                clientIndex.incrementAndGet();
                coolDown.set(r.nextInt(maxArrivalTimeDelta - minArrivalTimeDelta) + minArrivalTimeDelta);
            }

            try
            {
                Thread.sleep(1000);
            }

            catch (InterruptedException e)
            {
                Logger.getGlobal().log(Level.INFO, "Simulation interrupted".toUpperCase());
            }

        }

        List<Server> serv = scheduler.getServers();
        this.stop();

        this.gui.displayData(serversToString(), currentTime.get(), isDone.get());

        Logger.getGlobal().log(Level.INFO, "SIMULATION END");
        Logger.getGlobal().info("Statistics:");

        float avgOverallWaitTime = 0.0f;
        float avgOverallServiceTime = 0.0f;
        float avgOverallEmptyQTime = 0.0f;

        for (int i = 0; i < serv.size(); i++) {
            Server e = serv.get(i);

            avgOverallWaitTime += e.getData()[0];
            avgOverallServiceTime += e.getData()[1];
            avgOverallEmptyQTime += e.getData()[2];

            Logger.getGlobal().info("Server " + i);
            Logger.getGlobal().info("Average wait time: " + e.getData()[0]);
            Logger.getGlobal().info("Average service time: " + e.getData()[1]);
            Logger.getGlobal().info("Empty queue time: " + (int) e.getData()[2]);
        }

        Logger.getGlobal().log(Level.INFO, "Peak time: " + peakTime.get() + " with " + peakClients + " clients");
        Logger.getGlobal().info("Avg wait time overall: " + avgOverallWaitTime / (float) serv.size());
        Logger.getGlobal().info("Avg service time overall: " + avgOverallServiceTime / (float) serv.size());
        Logger.getGlobal().info("Avg empty queue time overall: " + avgOverallEmptyQTime / (float) serv.size());
    }

    private List<String> serversToString()
    {
        List<String> strs = new ArrayList<>();

        ListIterator<Server> it = scheduler.getServers().listIterator();

        while (it.hasNext())
        {
            strs.add(it.next().toString());
        }

        return strs;
    }

    public synchronized void stop()
    {
        scheduler.killThreads();
        isDone.set(true);
        currentTime.set(timeLimit);
    }

    private void updatePeakTime()
    {
        List<Server> servers = scheduler.getServers();

        int sum = 0;

        for(Server i : servers)
        {
            sum += i.getSize();
        }

        if(sum > peakClients.get())
        {
            peakClients.set(sum);
            peakTime.set(currentTime.get());
        }
    }
}
