package Main;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import Strategies.*;

public class GUI extends JFrame
{
    private int noServers;

    private JPanel panel;
    private JPanel dataPanel;
    private JPanel timePanel;
    private List<JTextArea> textAreas;
    private List<JLabel> labels;

    private SimManager simManager;

    private JTextField noServerText;
    private JTextField simTimeText;
    private JTextField minProcessingTimeText;
    private JTextField maxProcessingTimeText;
    private JTextField maxClientPerServerText;
    private JTextField minDeltaTimeArrivalText;
    private JTextField maxDeltaTimeArrivalText;
    private JComboBox<String> comboBox;

    private JButton startSimBtn;
    private JButton stopSimBtn;

    private JLabel timeLabel;
    private JLabel isDoneLabel;

    private Thread t;

    public GUI()
    {
        dataPanel = new JPanel();

        setTitle("Queue simulation");

        noServerText = new JTextField(2);
        dataPanel.add(new JLabel("noServers: "));
        dataPanel.add(noServerText);

        simTimeText = new JTextField(4);
        dataPanel.add(new JLabel("Simulation Limit: "));
        dataPanel.add(simTimeText);

        minProcessingTimeText = new JTextField(3);
        dataPanel.add(new JLabel("Min Processing time: "));
        dataPanel.add(minProcessingTimeText);

        maxProcessingTimeText = new JTextField(3);
        dataPanel.add(new JLabel("Max Processing time: "));
        dataPanel.add(maxProcessingTimeText);

        minDeltaTimeArrivalText = new JTextField(3);
        dataPanel.add(new JLabel("Min Arrival time between clients: "));
        dataPanel.add(minDeltaTimeArrivalText);

        maxDeltaTimeArrivalText = new JTextField(3);
        dataPanel.add(new JLabel("Max Arrival time between clients: "));
        dataPanel.add(maxDeltaTimeArrivalText);

        maxClientPerServerText = new JTextField(3);
        dataPanel.add(new JLabel("Max client per server: "));
        dataPanel.add(maxClientPerServerText);

        String[] cbstr = {"Shortest queue", "Shortest processing time"};
        comboBox = new JComboBox<String>(cbstr);
        dataPanel.add(new JLabel("Selection policy: "));
        dataPanel.add(comboBox);

        startSimBtn = new JButton("Go");
        startSimBtn.addActionListener(e ->
        {
            if(panel != null) this.remove(panel);
            if(simManager != null)
            {
                simManager.stop();
                t.interrupt();
            }
            initSIM();
        });

        stopSimBtn = new JButton("Stop Simulation");
        stopSimBtn.addActionListener(e ->
        {
            if(panel != null) this.remove(panel);
            if(simManager != null)
            {
                simManager.stop();
                t.interrupt();
            }
        });

        dataPanel.add(startSimBtn);
        dataPanel.add(stopSimBtn);
        dataPanel.setLayout(new FlowLayout());

        //this.setSize(600, 600);

        this.add(dataPanel);
        this.setLayout(new GridLayout(3, 1));
        //panel.setLocation(0, 400);

        this.setResizable(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        //this.setSize(new Dimension(600, 600));
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    private void initSIM()
    {
        if(panel != null) this.remove(panel);
        if(timeLabel != null) this.remove(timePanel);
        this.noServers = Integer.parseInt(noServerText.getText());
        this.textAreas = new ArrayList<>();
        this.labels = new ArrayList<>();
        this.timeLabel = new JLabel();

        this.panel = new JPanel();
        this.timePanel = new JPanel();

        for(int i = 0 ; i < noServers; i++)
        {
            labels.add(new JLabel("Server " + i));

            JTextArea temp = new JTextArea();
            temp.setRows(10);
            temp.setColumns(Integer.parseInt(maxClientPerServerText.getText()));
            textAreas.add(temp);
        }

        for (int i = 0; i < noServers; i++) {
            panel.add(labels.get(i));
            panel.add(textAreas.get(i));
        }

        panel.setLayout(new GridLayout(1, noServers));
        timePanel.add(new JLabel("Time: "));
        timePanel.add(timeLabel);
        isDoneLabel = new JLabel("Simulation Done");
        isDoneLabel.setVisible(false);
        timePanel.add(isDoneLabel);
        timePanel.setLayout(new FlowLayout());

        this.add(panel);
        this.add(timePanel);
        this.pack();
        this.setSize(800, 800);

        //initialise simulation manager
        if(simManager != null)
        {
            simManager.stop();
            t.interrupt();
        }

        int ind = comboBox.getSelectedIndex();
        Policy p;

        switch (ind)
        {
            case 0:
                p = Policy.SHORTEST_QUEUE;
                break;
            case 1:
                p = Policy.SHORTEST_SERVICE_TIME;
                break;
                default:
                    p = Policy.SHORTEST_QUEUE;
                    break;
        }

        simManager = new SimManager(this, Integer.parseInt(simTimeText.getText()), Integer.parseInt(minProcessingTimeText.getText()), Integer.parseInt(maxProcessingTimeText.getText()),
                Integer.parseInt(noServerText.getText()), Integer.parseInt(maxClientPerServerText.getText()), Integer.parseInt(minDeltaTimeArrivalText.getText()), Integer.parseInt(maxDeltaTimeArrivalText.getText()), p);
        t = new Thread(simManager);
        t.start();
    }

    public static void main(String[] args)
    {
        new GUI();
    }

    public void displayData(List<String> str, int time, boolean done)
    {
        for (int i = 0; i < str.size(); i++)
        {
            try
            {
                textAreas.get(i).setText(str.get(i));
            }
            catch (IndexOutOfBoundsException e)
            {
                e.printStackTrace();
            }
        }

        timeLabel.setText(Integer.toString(time));

        if(done)
        {
            isDoneLabel.setVisible(true);
        }

        else isDoneLabel.setVisible(false);
    }
}
