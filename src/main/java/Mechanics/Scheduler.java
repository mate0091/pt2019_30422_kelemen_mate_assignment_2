package Mechanics;

import Strategies.*;

import java.util.*;

public class Scheduler
{
    private List<Server> servers;

    private SelectionPolicy policy;

    private List<Thread> threads;

    public Scheduler(int maxNoServers, int maxTasksPerServer, Policy p)
    {
        this.servers = new ArrayList<>();
        this.threads = new ArrayList<>();

        if(p ==  Policy.SHORTEST_QUEUE)
        {
            this.policy = new ShortestQueuePolicy();
        }
        else if(p == Policy.SHORTEST_SERVICE_TIME)
        {
            this.policy = new ShortestServiceTimePolicy();
        }

        //create servers
        for(int i = 0; i < maxNoServers; i++)
        {
            Server e = new Server(maxTasksPerServer);
            this.servers.add(e);
        }

        //create thread with the object
        //iterate over list
        ListIterator<Server> it = this.servers.listIterator();

        while(it.hasNext())
        {
            Thread t = new Thread(it.next());
            t.start();
            threads.add(t);
        }
    }

    public void dispatchTask(Task t)
    {
        this.policy.addTask(servers, t);
    }

    public List<Server> getServers() {
        return servers;
    }

    public void killThreads()
    {
        for (Thread i : threads)
        {
            i.interrupt();
        }
    }
}
