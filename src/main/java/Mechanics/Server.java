package Mechanics;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

//implement one server and generate clients with concurrency

public class Server implements Runnable
{
    private LinkedBlockingQueue<Task> tasks;
    private AtomicInteger waitingPeriod;
    private AtomicInteger serviceTime;
    private AtomicInteger emptyQTime;
    private AtomicBoolean isRunning;

    private AtomicInteger clientsServed;

    public Server(int maxTasks)
    {
        tasks = new LinkedBlockingQueue<Task>(maxTasks);
        waitingPeriod = new AtomicInteger(0);
        serviceTime = new AtomicInteger(0);

        emptyQTime = new AtomicInteger(0);

        this.isRunning = new AtomicBoolean(false);
        this.clientsServed = new AtomicInteger(0);
    }

    public int addTask(Task newTask)
    {
        //add new task to queue
        //newTask.setWaitTime(this.waitingPeriod.get());
        if(tasks.remainingCapacity() == 0) return -1;

        else tasks.add(newTask);

        return 1;
    }

    public synchronized void run()
    {
        isRunning.set(true);

        while(isRunning.get())
        {
            if(tasks.peek() != null)
            {
                Task current = tasks.peek();

                try {
                    Thread.sleep(current.getProcessingTime() * 1000);
                    Logger.getGlobal().log(Level.INFO, "Client " + current.toString() + " Exited from Server " + current.getServerIndex());

                    waitingPeriod.addAndGet(current.getWaitTime());
                    serviceTime.addAndGet(current.getProcessingTime());

                    int temp = current.getProcessingTime();

                    tasks.remove(current);
                    tasks.forEach(e ->
                    {
                        e.incWaitTime(temp);
                    });

                    clientsServed.incrementAndGet();

                } catch (InterruptedException e) {
                    isRunning.set(false);
                }
            }
        }
    }

    public String toString()
    {
        StringBuilder s = new StringBuilder();

        for(Task i : tasks)
        {
            s.append(i.toString() + "\n");
        }

        return s.toString();
    }

    public int getSize()
    {
        return tasks.size();
    }

    public int getSumProcessingTime()
    {
        int sum = 0;

        Iterator<Task> it = tasks.iterator();

        while(it.hasNext())
        {
            sum += it.next().getProcessingTime();
        }

        return sum;
    }

    public void incEmptyQTime()
    {
        if(tasks.isEmpty())
        {
            emptyQTime.incrementAndGet();
        }
    }

    public float[] getData()
    {
        float avgWaitTime;
        float avgServiceTime;

        if(clientsServed.get() == 0)
        {
            avgServiceTime = 0.0f;
            avgWaitTime = 0.0f;
        }

        else
        {
            avgWaitTime = (float) waitingPeriod.get() / clientsServed.get();
            avgServiceTime = (float) serviceTime.get() / clientsServed.get();
        }
        float[] data = {avgWaitTime, avgServiceTime, (float) emptyQTime.get(), (float) clientsServed.get()};

        return data;
    }
}
