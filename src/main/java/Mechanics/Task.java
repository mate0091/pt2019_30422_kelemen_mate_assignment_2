package Mechanics;

import java.util.Random;

public class Task
{
    private int waitTime;
    private int processingTime;
    private int arrivalTime;

    private int serverIndex;
    private int arrivalIndex;

    public Task(int arrivalTime, int processingTime, int arrivalIndex)
    {
        this.arrivalTime = arrivalTime;
        this.waitTime = 0;
        this.processingTime = processingTime;
        this.serverIndex = -1;
        this.arrivalIndex = arrivalIndex;
    }

    public int getWaitTime() {
        return waitTime;
    }

    public int getProcessingTime()
    {
        return processingTime;
    }

    public void incWaitTime(int amount) {
        this.waitTime += amount;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public static Task random(int minProcessingTime, int maxProcessingTime, int arrivalTime, int arrivalIndex)
    {
        Random r = new Random();

        int result = r.nextInt(maxProcessingTime - minProcessingTime) + minProcessingTime;

        return new Task(arrivalTime, result, arrivalIndex);
    }

    public String toString()
    {
        return this.arrivalIndex + " (" + this.arrivalTime + " " + this.processingTime + " " + this.waitTime + ")";
    }

    public int getServerIndex() {
        return serverIndex;
    }

    public void setServerIndex(int serverIndex) {
        this.serverIndex = serverIndex;
    }

    public void setArrivalIndex(int arrivalIndex) {
        this.arrivalIndex = arrivalIndex;
    }
}
